/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package personaapp;

/**
 *
 * @author alberto
 */
public class Persona {
    
     private String nombre;
    private int edad;
    private String dni;
    private char sexo;
    private int peso;
    private int altura;

    public Persona() {
        generarDNI();
        
    }

    public Persona(String nombre, int edad, char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        comprobarSexo();
        generarDNI();
    }

    public Persona(String nombre, int edad, char sexo, int peso, int altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        // Hay que meter el metodo comprobar sexo para que si el sexo está mal lo corrija
        comprobarSexo();
        this.peso = peso;
        this.altura = altura;
        // Usamos el metodo generarDNI para que genera un dni NOTA: observar que no metemos el dni en el constructor
        generarDNI();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
        
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
        // modificamos el setter por si cambiamos el sexo y que tenga uno de los dos valores permitidos
        comprobarSexo();
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int calcularIMC () {
        int resultado;
        // Como la altura la metemos en centimetros divimos la altura entre 100 porque en la formula tiene que ir en metros
        int pesoIdeal = this.peso*(int)Math.pow(altura/100, 2);
        if (pesoIdeal<20){
            resultado=-1;
        } else if (20>=pesoIdeal && pesoIdeal<=25) {
            resultado=0;
        } else {
            resultado=1;
            
        }
        return resultado;
    }
    
    public boolean esMayorDeEdad () {
        if (this.edad>=18) {
            return true;
        } else {
        return false;
        }
    }
    
    private void comprobarSexo () {
        // el signo de la exclamacion es para negar ( hace lo opuesto que lo que sta dentro del parentesis)
        if (this.sexo!='H' && this.sexo!='M'){
            this.sexo = 'H';
            // con el setter setSexo('H');
            
        }
    }
    
    private void generarDNI () {
        //Generamos un numero aleatorio de 8 cifras
        int dni = (int)(Math.random()*99999999-10000000+1)+10000000;
        //Sacar la letra del DNI (cogemos la forma de forma de hacerlo de internet)
        String caracteres="TRWAGMYFPDXBNJZSQVHLCKE";
          int resto = dni%23;
          // Extraemos la letra del array caracteres
           char letra= caracteres.charAt(resto);
           
           // Convertimos la letra (char) en String
           String letraString = Character.toString(letra);
           // Convertimos el dni (int) en String
           String dniString = String.valueOf(dni); 
           // Concatenamos los dos Strings (numero y letra) para tener el DNI
           String dnitexto;
           dnitexto= dniString.concat(letraString);
           // Asignamos el valor del dni calculado a la propiedad dni
           this.dni= dnitexto;

    }
    
    @Override
    public String toString() {
        return "El nombre: " + nombre + " , la edad: " + edad + " , el dni: " + dni + " , el sexo es: " + sexo + " , el peso es: " + peso + " , la altura: " + altura;
    }

}
