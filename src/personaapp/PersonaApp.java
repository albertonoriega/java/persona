/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package personaapp;

import javax.swing.JOptionPane;

/**
 *
 * @author alberto
 */
public class PersonaApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String nombre, sexoString, edadString, pesoString, alturaString;
        int edad, peso, altura;
        char sexo;
        
        
        
        Persona persona1, persona2, persona3;
        
     
        // Constructor que obtiene las variables introducidas por teclado
        persona1 = new Persona(pedirNombre(), pedirEdad(), pedirSexo(), pedirAltura(), pedirPeso());
        // Constructor con solo nombre, edad y sexo introducidas por teclado por el usuario           
        persona2 = new Persona(pedirNombre(), pedirEdad(), pedirSexo());
        
        // Constructor por defecto, Introducimos las propiedades con los setter
        persona3 = new Persona();
        persona3.setNombre(pedirNombre());
        persona3.setEdad(pedirEdad());
        persona3.setSexo(pedirSexo());
        persona3.setAltura(pedirAltura());
        persona3.setPeso(pedirPeso());
        
        // Metodo IMC
        System.out.println(persona1.calcularIMC());
        System.out.println(persona2.calcularIMC());
        System.out.println(persona3.calcularIMC());
        
        // Comprobamos la edad
        System.out.println(persona1.esMayorDeEdad());
        System.out.println(persona2.esMayorDeEdad());
        System.out.println(persona3.esMayorDeEdad());
        
        // Metodo toString
        System.out.println(persona1.toString());
        System.out.println(persona2.toString());
        System.out.println(persona3.toString());
    }
    
    // Método que te pide el nombre
    public static String pedirNombre () {
        String nombre;
    // Pedimos por teclado el nombre (String)
        nombre = JOptionPane.showInputDialog("Escriba el nombre");
        return nombre;
    }
    
    // Método que te pide el sexo
    public static char pedirSexo () {
        String sexoString;
        char sexo;
        // pedimos por teclado el sexo (char)
        sexoString = JOptionPane.showInputDialog("Escriba el sexo");
        // Como es String lo tenemos que pasar a char
        sexo= sexoString.charAt(0);
        // Pasamos el char a mayusculas. Si el usuario introduce m o h también sean valores válidos
        sexo = Character.toUpperCase(sexo);
        return sexo;
    }
    
    // Método que te pide la edad
    public static int pedirEdad () {
        String edadString; 
        int edad;
        // Pedimos por teclado la edad (int)
        edadString = JOptionPane.showInputDialog("Escriba la edad");
        // Como es String lo tenemos que pasar a int
        edad = Integer.parseInt(edadString);
        return edad;
    }
    
    // Metodo que te pide la altura
    public static int pedirAltura () {
        String alturaString; 
        int altura;
        // Pedimos por teclado la altura (int)
        alturaString = JOptionPane.showInputDialog("Escriba la altura");
        // Como es String lo tenemos que pasar a int
        altura = Integer.parseInt(alturaString);
        return altura;
    }
    
    // Metodo que te pide el peso
    public static int pedirPeso () {
        String pesoString; 
        int peso;
        // Pedimos por teclado el peso (int)
        pesoString = JOptionPane.showInputDialog("Escriba el peso");
        // Como es String lo tenemos que pasar a int
        peso = Integer.parseInt(pesoString);
        return peso;
    }

    }
    

